var letras = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E', 'T'];
var numero = prompt("Número del DNI");


    if (numero < 0 || numero > 99999999){
        console.log("El número no es válido.")
    }
    else {
        var letraUsuario = prompt("Letra");
        var letraCorrecta = letras[numero % 23]
        if (letraCorrecta !== letraUsuario){
            console.log("La letra no es correcta")
        }
        else{
            console.log("DNI válido")
        }
    }


